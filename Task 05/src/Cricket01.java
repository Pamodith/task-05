import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Cricket01 {
  public static void main(String[] args) {
    List<Cricket_Club> table = Arrays.asList(
        new Cricket_Club(1, "Colombo Cricket Club", 23, 17, 10, 45, 31, 10, 121, 85, -235,
            8, 2, 76),
        new Cricket_Club(2, "Gall Cricket Club", 12, 26, 20, 9, 63, 48, 258, 74, -469, 39, 12, 55),
        new Cricket_Club(3, "NCC", 2, 15, 1, 6, 453, 421, 32, 37, 39, 4,
            2, 48),
        new Cricket_Club(4, "Kaluthara CC", 22, 14, 1, 7, 64, 18, 2, 70, 11, 5, 5, 37),
        new Cricket_Club(5, "Kaduwela CC", 22, 14, 0, 8, 63, 7, 226, 70, 1, 5, 7,
            52),
        new Cricket_Club(6, "Jafna Stallian", 10, 11, 2, 9, 62, 27, 145, 77, -18, 9, 4, 61),
        new Cricket_Club(7, "Kandy Falcons", 27, 19, 0, 21, 97, 28, 34, 62, 7, 6, 4,
            54),
        new Cricket_Club(8, "Dambulla Giants", 15, 10, 0, 42, 44, 4, -60, 45, 40, 4, 5,
            49),
        new Cricket_Club(9, "Huskers Cricket Club", 27, 9, 1, 12, 93, 5, -22, 53, 7, 4, 6,
            48),
        new Cricket_Club(10, "JCC", 22, 7, 1, 14, 42, 2, 6, 46, 7, 7, 6,
            40),
        new Cricket_Club(11, "Malabe Cricket Club", 10, 9, 12, 16, 47, 5, -54, 57, 11,
            4, 8, 34),
        new Cricket_Club(12, "Matara Cricket Club", 15, 0, 0, 22, 23, 21, -798, 29, 1, 1,
            0, 1));

     table.forEach(x -> System.out.println(x));
     try {
			FileWriter writer = new FileWriter("Text01.txt");
			writer.write("   Team Name                   won      drawn     lost    \n");
			writer.write("   ---------                   ---      ------    ----    \n");
			table.forEach(x -> {
			try {
				writer.write(x + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
			writer.close();
			System.out.println("\nTXT file wrote successfuly!");
	    } catch (IOException e) {
	    	System.out.println("Error!");
	    	e.printStackTrace();
	    }
	}

  

}
