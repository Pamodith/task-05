import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class Cricket03 {
  public static void main(String[] args) {
    List<Cricket_Club> table = Arrays.asList(
    		 new Cricket_Club(1, "Colombo Cricket Club", 23, 17, 10, 45, 31, 10, 121, 85, -235,
    		            8, 2, 76),
    		        new Cricket_Club(2, "Gall Cricket Club", 12, 26, 20, 9, 63, 48, 258, 74, -469, 39, 12, 55),
    		        new Cricket_Club(3, "NCC", 2, 15, 1, 6, 453, 421, 32, 37, 39, 4,
    		            2, 48),
    		        new Cricket_Club(4, "Kaluthara CC", 22, 14, 1, 7, 64, 18, 2, 70, 11, 5, 5, 37),
    		        new Cricket_Club(5, "Kaduwela CC", 22, 14, 0, 8, 63, 7, 226, 70, 1, 5, 7,
    		            52),
    		        new Cricket_Club(6, "Jafna Stallian", 10, 11, 2, 9, 62, 27, 145, 77, -18, 9, 4, 61),
    		        new Cricket_Club(7, "Kandy Falcons", 27, 19, 0, 21, 97, 28, 34, 62, 7, 6, 4,
    		            54),
    		        new Cricket_Club(8, "Dambulla Giants", 15, 10, 0, 42, 44, 4, -60, 45, 40, 4, 5,
    		            49),
    		        new Cricket_Club(9, "Huskers Cricket Club", 27, 9, 1, 12, 93, 5, -22, 53, 7, 4, 6,
    		            48),
    		        new Cricket_Club(10, "JCC", 22, 7, 1, 14, 42, 2, 6, 46, 7, 7, 6,
    		            40),
    		        new Cricket_Club(11, "Malabe Cricket Club", 10, 9, 12, 16, 47, 5, -54, 57, 11,
    		            4, 8, 34),
    		        new Cricket_Club(12, "Matara Cricket Club", 15, 0, 0, 22, 23, 21, -798, 29, 1, 1,
    		            0, 1));


    OptionalInt max = table.stream().mapToInt(Cricket_Club::getPoints).max();
    OptionalInt min = table.stream().mapToInt(Cricket_Club::getPoints).min();
    if (min.isPresent()) {
        System.out.printf("Minimum number of points is %d\n", min.getAsInt());
      } else {
        System.out.println("min failed");
      }
    if (max.isPresent()) {
      System.out.printf("Maximum number of points is %d\n", max.getAsInt());
    } else {
      System.out.println("max failed");
    }
    
    List<String> obj1 = table.stream()
    	    .filter(p -> p.getLost() < 10)
    	    .map(Cricket_Club::getClub)
    	    .collect(Collectors.toList());
    	                   
    	System.out.println("Teams with less than 10 losses: " + obj1.toString());
    	
    	System.out.print("Number of matches drawn: ");
	    Integer rslt = table.stream().map(Cricket_Club::getDrawn).reduce(0, (a, b) -> a + b);
	    System.out.println(rslt);
    	
    try {
	      FileWriter writer = new FileWriter("Text03.txt");
	      writer.write("Highest number of points is " + max.getAsInt() + "\n");
	      writer.write("Highest number of points is " + min.getAsInt() + "\n");
	      writer.write("Number of matches drawn: " + rslt + "\n");
	      writer.write("Teams with less than 10 losses: " + obj1 + "\n");
	      writer.close();
	      System.out.println("\nTXT file wrote successfuly!");
	    } catch (IOException e) {
	      System.out.println("Error!");
	      e.printStackTrace();
	    }

  }

}
